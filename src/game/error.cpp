/*
 * Diskenn
 * Copyright © 2018, 2020 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "error.hpp"

#include <SDL2/SDL.h>

sdl_exception::sdl_exception(const std::string &message)
	: std::runtime_error(message + " [" + SDL_GetError() + "]")
{
}

sdl_exception::~sdl_exception() noexcept
{
}

const char *sdl_exception::what() const noexcept
{
	return std::runtime_error::what();
}
