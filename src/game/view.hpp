/*
 * Diskenn
 * Copyright © 2018, 2021 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VIEW_H
#define VIEW_H

class Block;
class Shape;

struct SDL_Window;

class Scene;

class View
{
public:
	View(const Scene &);
	~View();

	void initialize();
	void close();
	void set_fullscreen(bool);
	void toggle_fullscreen();
	void render();
	void swap_window();
	void change_size(int x, int y);

private:
	void set_projection();
	void set_lighting();

	void draw_block(const Block &block);
	void draw_next_shape(const Shape &);

	const Scene &scene;

	SDL_Window *sdl_window = nullptr;

	bool fullscreen_mode = false;
	bool viewport_ok = true;
	int viewport_width;
	int viewport_height;
	int viewport_offset_x;
	int viewport_offset_y;
	double board_angle;
};

#endif
