/*
 * Diskenn
 * Copyright © 2018, 2021 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLOCK_HPP
#define BLOCK_HPP

#include "defs.hpp"

struct Color
{
	double r, g, b;

	explicit Color() :
		r(0.0), g(0.0), b(0.0) {
	}

	Color(double red, double green, double blue) :
		r(red), g(green), b(blue) {
	}
};

struct Block {
	int x;
	int y;
	int z;
	Color color;
	BlockStyle style;

	explicit Block() :
		x(0), y(0), z(0), color(),
		style(BlockStyle::BS_QUAD) {
	}

	Block(int bx, int by, int bz, Color c, BlockStyle s) :
		x(bx), y(by), z(bz), color(c), style(s) {
	}
};

#endif
