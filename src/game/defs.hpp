/*
 * Diskenn
 * Copyright © 2018, 2021 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEFS_HH
#define DEFS_HH

#include "SDL.h"

#define BIT(n) (1UL << (n))

// Stringify preprocessor parameter
#define STR_(X) #X
#define STR(X) STR_(X)

typedef Uint32 gametime;

enum UserCommand {
	CMD_NONE,
	CMD_EXIT,
	CMD_FULLSCREEN,
	CMD_PAUSE,
	CMD_LEFT,
	CMD_RIGHT,
	CMD_DOWN,
	CMD_DROP,
	CMD_ROTATE_LEFT
};

enum GameState {
	GS_START,
	GS_PLAY,
	GS_PLAY_NEXT,
	GS_TURN,
	GS_END,
};

enum BlockStyle {
	BS_QUAD,
	BS_TRIANGLE,
};

#endif
