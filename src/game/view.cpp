/*
 * Diskenn
 * Copyright © 2018, 2020, 2021 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "view.hpp"

#include "config.h"
#include "defs.hpp"
#include "error.hpp"
#include "logger.hpp"
#include "scene.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include <cassert>

struct form
{
	std::vector<GLfloat> quads;
	std::vector<GLfloat> triangles;
};

const int LOGICAL_WIDTH = 190;
const int LOGICAL_HEIGHT = 260;
const int DEFAULT_WIDTH = LOGICAL_WIDTH * 2;
const int DEFAULT_HEIGHT = LOGICAL_HEIGHT * 2;
const int MINIMUM_WIDTH = LOGICAL_WIDTH;
const int MINIMUM_HEIGHT = LOGICAL_HEIGHT;
const int CLIP_NEAR = -2 * LOGICAL_WIDTH;
const int CLIP_FAR = 2 * LOGICAL_WIDTH;
const int BOARD_Z_OFFSET = -10;
const int BLOCK_WIDTH = 10;

const GLfloat EPS = 0.1;
const GLfloat EX1 = 0.0;
const GLfloat EX2 = 1.0;
const GLfloat EY1 = 0.0;
const GLfloat EY2 = 1.0;
const GLfloat EM1 = 0.5;
const GLfloat IX1 = EX1 + EPS;
const GLfloat IX2 = EX2 - EPS;
const GLfloat IY1 = EY1 + EPS;
const GLfloat IY2 = EY2 - EPS;
const GLfloat Z0 = 0.0;
const GLfloat Z1 = 1.0;
const GLfloat Z2 = Z1 + EPS;
const GLfloat Z3 = Z0 - EPS;

const form shape_form = {
	{
		IX1, IY1, Z2,  0.0,  0.0,  1.0, // Front
		IX2, IY1, Z2,  0.0,  0.0,  1.0,
		IX2, IY2, Z2,  0.0,  0.0,  1.0,
		IX1, IY2, Z2,  0.0,  0.0,  1.0,
		EX1, EY1, Z1,  0.0, -1.0,  1.0,
		EX2, EY1, Z1,  0.0, -1.0,  1.0,
		IX2, IY1, Z2,  0.0, -1.0,  1.0,
		IX1, IY1, Z2,  0.0, -1.0,  1.0,
		EX2, EY1, Z1,  1.0,  0.0,  1.0,
		EX2, EY2, Z1,  1.0,  0.0,  1.0,
		IX2, IY2, Z2,  1.0,  0.0,  1.0,
		IX2, IY1, Z2,  1.0,  0.0,  1.0,
		IX1, IY2, Z2,  0.0,  1.0,  1.0,
		IX2, IY2, Z2,  0.0,  1.0,  1.0,
		EX2, EY2, Z1,  0.0,  1.0,  1.0,
		EX1, EY2, Z1,  0.0,  1.0,  1.0,
		EX1, EY2, Z1, -1.0,  0.0,  1.0,
		EX1, EY1, Z1, -1.0,  0.0,  1.0,
		IX1, IY1, Z2, -1.0,  0.0,  1.0,
		IX1, IY2, Z2, -1.0,  0.0,  1.0,

		EX2, EY1, Z1,  1.0,  0.0,  0.0, // Right
		EX2, EY1, Z0,  1.0,  0.0,  0.0,
		EX2, EY2, Z0,  1.0,  0.0,  0.0,
		EX2, EY2, Z1,  1.0,  0.0,  0.0,

		EX1, EY1, Z1, -1.0,  0.0,  0.0, // Left
		EX1, EY2, Z1, -1.0,  0.0,  0.0,
		EX1, EY2, Z0, -1.0,  0.0,  0.0,
		EX1, EY1, Z0, -1.0,  0.0,  0.0,

		IX1, IY1, Z3,  0.0,  0.0, -1.0, // Back
		IX1, IY2, Z3,  0.0,  0.0, -1.0,
		IX2, IY2, Z3,  0.0,  0.0, -1.0,
		IX2, IY1, Z3,  0.0,  0.0, -1.0,
		EX1, EY1, Z0,  0.0, -1.0, -1.0,
		IX1, IY1, Z3,  0.0, -1.0, -1.0,
		IX2, IY1, Z0,  0.0, -1.0, -1.0,
		EX2, EY1, Z3,  0.0, -1.0, -1.0,
		EX2, EY1, Z0, -1.0,  0.0, -1.0,
		IX2, IY1, Z3, -1.0,  0.0, -1.0,
		IX2, IY2, Z3, -1.0,  0.0, -1.0,
		EX2, EY2, Z0, -1.0,  0.0, -1.0,
		IX1, IY2, Z3,  0.0,  1.0, -1.0,
		EX1, EY2, Z0,  0.0,  1.0, -1.0,
		EX2, EY2, Z0,  0.0,  1.0, -1.0,
		IX2, IY2, Z3,  0.0,  1.0, -1.0,
		EX1, EY2, Z0,  1.0,  0.0, -1.0,
		IX1, IY2, Z3,  1.0,  0.0, -1.0,
		IX1, IY1, Z3,  1.0,  0.0, -1.0,
		EX1, EY1, Z0,  1.0,  0.0, -1.0,
	},
	{ }
};

const form border_form = {
	{
		EX2, EY1, Z1,  1.0,  0.0,  0.0, // Right
		EX2, EY1, Z0,  1.0,  0.0,  0.0,
		EX2, EY2, Z0,  1.0,  0.0,  0.0,
		EX2, EY2, Z1,  1.0,  0.0,  0.0,

		EX1, EY1, Z1, -1.0,  0.0,  0.0, // Left
		EX1, EY2, Z1, -1.0,  0.0,  0.0,
		EX1, EY2, Z0, -1.0,  0.0,  0.0,
		EX1, EY1, Z0, -1.0,  0.0,  0.0,
	}, {
		EX1, EY1, Z1,  0.0, -0.5,  1.0, // Front
		EX2, EY1, Z1,  0.0, -0.5,  1.0,
		EM1, EM1, Z2,  0.0,  0.0,  1.0,
		EX2, EY1, Z1,  0.5,  0.0,  1.0,
		EX2, EY2, Z1,  0.5,  0.0,  1.0,
		EM1, EM1, Z2,  0.0,  0.0,  1.0,
		EX2, EY2, Z1,  0.0,  0.5,  1.0,
		EX1, EY2, Z1,  0.0,  0.5,  1.0,
		EM1, EM1, Z2,  0.0,  0.0,  1.0,
		EX1, EY2, Z1, -0.5,  0.0,  1.0,
		EX1, EY1, Z1, -0.5,  0.0,  1.0,
		EM1, EM1, Z2,  0.0,  0.0,  1.0,

		EX1, EY1, Z0,  0.0, -0.5, -1.0, // Back
		EM1, EM1, Z3,  0.0,  0.0, -1.0,
		EX2, EY1, Z0,  0.0, -0.5, -1.0,
		EX2, EY1, Z0, -0.5,  0.0, -1.0,
		EM1, EM1, Z3,  0.0,  0.0, -1.0,
		EX2, EY2, Z0, -0.5,  0.0, -1.0,
		EX2, EY2, Z0,  0.0,  0.5, -1.0,
		EM1, EM1, Z3,  0.0,  0.0, -1.0,
		EX1, EY2, Z0,  0.0,  0.5, -1.0,
		EX1, EY2, Z0,  0.5,  0.0, -1.0,
		EM1, EM1, Z3,  0.0,  0.0, -1.0,
		EX1, EY1, Z0,  0.5,  0.0, -1.0,
	}
};

static Logger logger(__FILE__);

/**
 * Constructor.
 */
View::View(const Scene &scene_) :
	scene(scene_)
{
	viewport_width = DEFAULT_WIDTH;
	viewport_height = DEFAULT_HEIGHT;
	viewport_offset_x = 0;
	viewport_offset_y = 0;
	board_angle = 0.0;
}

/**
 * Destructor. Close window.
 */
View::~View()
{
	close();
}

/**
 * Initialize SDL window with OpenGL context
 */
void View::initialize()
{
	logger.debug(__func__);

	// SDL
	int flags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE;

	if (fullscreen_mode) {
		flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}

	sdl_window = SDL_CreateWindow(
		STR(PROJECT_VERSION),
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		DEFAULT_WIDTH,
		DEFAULT_HEIGHT,
		flags);

	if (sdl_window == nullptr)
		throw sdl_exception("SDL window creation failed.");

	auto sdl_renderer = SDL_CreateRenderer(sdl_window, -1, 0);
	if (sdl_renderer == nullptr)
		throw sdl_exception("SDL renderer creation failed.");

	auto gl_context = SDL_GL_CreateContext(sdl_window);
	if (gl_context == nullptr)
		throw sdl_exception("SDL OpenGL context creation failed.");

	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
}

/**
 * Set viewport and projection matrix
 */
void View::set_projection()
{
	glViewport(viewport_offset_x, viewport_offset_y,
		   viewport_width, viewport_height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, LOGICAL_WIDTH, 0, LOGICAL_HEIGHT, CLIP_NEAR, CLIP_FAR);
}

/**
 * Setup light and
 */
void View::set_lighting()
{
	GLfloat specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat shininess[] = { 90.0 };
	GLfloat x = 0.5 * LOGICAL_WIDTH;
	GLfloat y = 1.1 * LOGICAL_HEIGHT;
	GLfloat z = 0.4 * LOGICAL_HEIGHT;
	GLfloat light0[4] = { x, y, z, 1.0 };

	glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glLightfv(GL_LIGHT0, GL_POSITION, light0);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
}

/**
 * Close SDL window
 */
void View::close()
{
	set_fullscreen(false);
	if (sdl_window != nullptr) {
		SDL_Delay(100);
		SDL_DestroyWindow(sdl_window);
		sdl_window = nullptr;
	}
}

/**
 * Render game view
 */
void View::render()
{
	assert(sdl_window);

	board_angle = scene.get_board_angle();

	set_projection();
	set_lighting();

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (!viewport_ok)
		return;

	glShadeModel(GL_SMOOTH);
	glEnable(GL_NORMALIZE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);

	glPushMatrix();

	glTranslatef(130 / 2, 0.0, 0.0);
	glRotatef(board_angle, 0.0, 1.0, 0.0);
	glTranslatef(130 / -2, 0.0, 0.0);

	for (auto &block : scene.get_blocks())
		draw_block(block);

	glPopMatrix();

	auto shapes = scene.get_next_shapes();
	if (shapes.size() > 0)
		draw_next_shape(shapes.front());
}

/**
 * Swap window
 */
void View::swap_window()
{
	SDL_GL_SwapWindow(sdl_window);
}

/**
 * Enable or disable fullscreen mode
 */
void View::set_fullscreen(bool fullscreen)
{
	logger.debug(__func__);

	if (!sdl_window)
		return;

	int flags = fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0;
	int status = SDL_SetWindowFullscreen(sdl_window, flags);

	if (status < 0)
		logger.warning("SDL Fullscreen toggle failed. [%s]",
			       SDL_GetError());
}

/**
 * Reset viewport dimensions/location after window size changes
 */
void View::change_size(int x, int y)
{
	logger.trace("Change window size: %dx%d", x, y);

	assert(x >= 0);
	assert(y >= 0);

	if (x < MINIMUM_WIDTH || y < MINIMUM_HEIGHT) {
		viewport_ok = false;
		return;
	}

	viewport_ok = true;
	double ratio_xy = (double) MINIMUM_WIDTH / MINIMUM_HEIGHT;

	if ((double) x / y > ratio_xy) {
		viewport_height = y;
		viewport_width = viewport_height * ratio_xy;
		viewport_offset_x = (x - viewport_width) / 2;
		viewport_offset_y = 0;
	} else {
		viewport_width = x;
		viewport_height = viewport_width / ratio_xy;
		viewport_offset_y = (y - viewport_height) / 2;
		viewport_offset_x = 0;
	}
}

/**
 * Render one block
 */
void View::draw_block(const Block &block)
{
	double shade = std::abs(std::sin(board_angle * M_PI / 360.0)) * 0.9 ;
	double dim = block.z == 1 ? 1.0 - shade : 0.1 + shade;

	int stride = 6 * sizeof(GLfloat);

	const form *arrays = nullptr;

	switch (block.style) {
	case BS_QUAD:
		arrays = &shape_form;
		break;
	case BS_TRIANGLE:
		arrays = &border_form;
		break;
	default: assert(!"Unhandled block style");
	}

	glPushMatrix();

	glTranslatef(block.x * BLOCK_WIDTH,
		     block.y * BLOCK_WIDTH,
		     block.z * BLOCK_WIDTH + BOARD_Z_OFFSET);

	glScalef(BLOCK_WIDTH, BLOCK_WIDTH, BLOCK_WIDTH);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glColor4d(block.color.r * dim,
		  block.color.g * dim,
		  block.color.b * dim,
		  1.0);

	int size = arrays->quads.size() / 6; // 2 vectors per entry
	if (size > 0) {
		glVertexPointer(3, GL_FLOAT, stride, arrays->quads.data());
		glNormalPointer(GL_FLOAT, stride, arrays->quads.data() + 3);
		glDrawArrays(GL_QUADS, 0, size);
	}

	size = arrays->triangles.size() / 6;
	if (size > 0) {
		glVertexPointer(3, GL_FLOAT, stride, arrays->triangles.data());
		glNormalPointer(GL_FLOAT, stride, arrays->triangles.data() + 3);
		glDrawArrays(GL_TRIANGLES, 0, size);
	}

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	glPopMatrix();
}

/**
 * Draw the next shape
 */
void View::draw_next_shape(const Shape &shape)
{
	assert(shape.z == 0 || shape.z == 1);

	set_lighting();

	for (auto block : shape.blocks) {
		if (shape.z == 1)
			block.x = 14 - shape.left_offset() + block.x;
		else
			block.x = 14 + shape.right_offset() - block.x;

		block.y = 24 - shape.top_offset() + block.y;
		block.z = shape.z;
		draw_block(block);
	}
}

/**
 * Toggle between window mode and fullscreen mode
 */
void View::toggle_fullscreen()
{
	fullscreen_mode = !fullscreen_mode;
	set_fullscreen(fullscreen_mode);
}
