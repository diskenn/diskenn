/*
 * Diskenn
 * Copyright © 2018, 2021 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SHAPE_HPP
#define SHAPE_HPP

#include "block.hpp"
#include "defs.hpp"

#include <queue>
#include <vector>

struct Shape {
	int left_offset() const;
	int right_offset() const;
	int top_offset() const;
	int width() const;

	int x = 0;
	int y = 0;
	int z = 0;
	std::vector<Block> blocks;
	gametime time = 0;
	gametime last_fall = 0;
	bool may_rotate = true;
};

void push_shuffled_shapes(std::queue<Shape> &shapes);

#endif
