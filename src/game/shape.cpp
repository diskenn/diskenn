/*
 * Diskenn
 * Copyright © 2018, 2021 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "shape.hpp"

#include <algorithm>
#include <cstdlib>
#include <limits>
#include <queue>
#include <random>

struct shape_description {
	std::vector<std::pair<int, int>> blocks;
	bool may_rotate = true;
};

static const std::vector<shape_description> classic_set = {
	{ { { -1, 0 }, {  0,  0 }, { -1, -1 }, {  0, -1 } }, false, }, // O
	{ { {  0, 2 }, {  0,  1 }, {  0,  0 }, {  0, -1 } } }, // I
	{ { { -1, 1 }, { -1,  0 }, {  0,  0 }, {  0, -1 } } }, // S
	{ { {  1, 1 }, {  1,  0 }, {  0,  0 }, {  0, -1 } } }, // Z
	{ { {  0, 1 }, {  0,  0 }, {  0, -1 }, {  1, -1 } } }, // L
	{ { {  0, 1 }, {  0,  0 }, {  0, -1 }, { -1, -1 } } }, // J
	{ { {  0, 1 }, { -1,  0 }, {  0,  0 }, {  1,  0 } } }, // T
};

static const std::vector<shape_description> split_set = {
	{ { { -1, 1 }, { -1,  0 }, {  1,  0 }, {  1, -1 } } },
	{ { { -1, 0 }, { -1, -1 }, {  1,  1 }, {  1,  0 } } },
	{ { { -1, 1 }, {  0,  1 }, {  1,  1 }, {  0, -1 } } },
	{ { { -2, 0 }, { -1,  0 }, {  1,  0 }, {  2,  0 } } },
	{ { { -1, 1 }, {  0,  1 }, {  1,  1 }, { -1, -1 } } },
	{ { { -1, 1 }, {  0,  1 }, {  1,  1 }, {  1, -1 } } },
};

static const std::vector<shape_description> easy_set = {
	{ { { -1,  0 }, {  0,  0 }, {  1,  0 } } },
	{ { { -1,  0 }, {  0,  0 }, {  0, -1 } } },
};

static std::default_random_engine random_engine(time(nullptr));

/**
 * Fill the given queue with a shuffled set of shapes.
 *
 * Only the blocks are created, other attributes are not set.
 */
void push_shuffled_shapes(std::queue<Shape> &shapes)
{
	auto &shape_set = classic_set;

	auto n = shape_set.size();
	std::vector<int> series(n);
	std::iota(series.begin(), series.end(), 0);
	std::shuffle(series.begin(), series.end(), random_engine);

	for (auto i : series) {
		Color color = {
			(((i + 1) & 4) >> 2) * 0.5 + 0.5,
			(((i + 1) & 2) >> 1) * 0.5 + 0.5,
			(((i + 1) & 1) >> 0) * 0.5 + 0.5,
		};
		auto description = shape_set[i];

		Shape shape;

		for (auto &p : description.blocks) {
			Block block;
			block.x = p.first;
			block.y = p.second;
			block.z = 0;
			block.color = color;
			block.style = BS_QUAD;
			shape.blocks.push_back(block);
		}
		shape.may_rotate = description.may_rotate;

		shapes.push(shape);
	}
}

/**
 * Get vertical offset of highest block
 */
int Shape::top_offset() const
{
	int offset = std::numeric_limits<int>::min();
	for (auto &block : blocks)
		offset = std::max(offset, block.y);

	return offset;
}

/**
 * Get horizontal offset on left side of block
 */
int Shape::left_offset() const
{
	int offset = std::numeric_limits<int>::max();
	for (auto &block : blocks)
		offset = std::min(offset, block.x);

	return offset;
}

/**
 * Get horizontal offset on right side of block
 */
int Shape::right_offset() const
{
	int offset = std::numeric_limits<int>::min();
	for (auto &block : blocks)
		offset = std::max(offset, block.x);

	return offset;
}

/**
 * Get horizontal offset on left side of block
 */
int Shape::width() const
{
	int x1 = std::numeric_limits<int>::max();
	int x2 = std::numeric_limits<int>::min();

	for (auto &block : blocks) {
		x1 = std::min(x1, block.x);
		x2 = std::max(x2, block.x);
	}
	return x2 - x1 + 1;
}
