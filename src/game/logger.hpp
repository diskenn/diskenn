/*
 * Diskenn
 * Copyright © 2018 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOGGER_H
#define LOGGER_H

#include <string>

class Logger
{
public:
	enum Level { ERROR, WARNING, INFO, DEBUG, TRACE };

	static void set_global_level(Level level);

	Logger(const std::string &id);

	void error(const char *, ...) const;
	void warning(const char *, ...) const;
	void info(const char *, ...) const;
	void debug(const char *, ...) const;
	void trace(const char *, ...) const;

private:
	static std::string basename_without_extension(const std::string &);
	static bool should_log_trace_level(const std::string &id);

	static int global_level;

	void log_message(Level level, const char *format, va_list args) const;

	const std::string log_id;
	const bool log_trace_level;
};

#endif
