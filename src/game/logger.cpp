/*
 * Diskenn
 * Copyright © 2018, 2021 Benoît Bréholée
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "logger.hpp"

#include "defs.hpp"

#include <algorithm>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>

/* Global log level used to filter messages */
int Logger::global_level = Logger::INFO;

/**
 * Logger constructor
 *
 * \param id log identifier to use as prefix and to filter
 */
Logger::Logger(const std::string &id) :
	log_id(basename_without_extension(id)),
	log_trace_level(should_log_trace_level(log_id))
{
}

/**
 * Set global log level
 */
void Logger::set_global_level(Level value)
{
	global_level = value;
}

/**
 * Remove path and suffix from filename if any one exists.
 *
 * Platform-specific.
 */
std::string Logger::basename_without_extension(const std::string &filename)
{
	size_t index = filename.rfind("/");
	index = (index == std::string::npos) ? 0 : index + 1;
	std::string base = filename.substr(index);

	index = base.rfind(".");
	return index != std::string::npos ? base.substr(0, index) : base;
}

/**
 * Find whether logs of level TRACE should be written. These logs are
 * only activated if environment variable <project>_TRACE contains the id
 * of the logger (provided as parameter). The variable may contain
 * several ids separated by colons.
 */
bool Logger::should_log_trace_level(const std::string &id)
{
	std::string trace_variable = STR(PROJECT_NAME);
	std::transform(
		trace_variable.begin(),
		trace_variable.end(),
		trace_variable.begin(),
		::toupper);
	trace_variable += "_TRACE";

	const char *c_str = std::getenv(trace_variable.c_str());
	if (c_str == nullptr)
		return false;

	std::string s(c_str);

	size_t index = s.find(id);
	if (index == std::string::npos)
		return false;

	if (index != 0 && s[index - 1] != ':')
		return false;

	if (index + id.size() != s.size() && s[index + id.size()] != ':')
		return false;

	return true;
}

/**
 * Log message to stdout if the level is below the current threshold
 *
 * \param level   Level of the log message
 * \param format  Format of the message (printf-like)
 * \param args    Arguments for the format
 */
void Logger::log_message(Level level, const char *format, va_list args) const
{
	if (level > global_level)
		return;

	if (level == TRACE && !log_trace_level)
		return;

	if (format == nullptr)
		return;

	auto &stream = stdout;

	if (global_level >= DEBUG)
		fprintf(stream, "%8s| ", log_id.c_str());

	std::string message(format);
	message += "\n";
	vfprintf(stream, message.c_str(), args);
}

/**
 * Log message at 'info' level
 * \param format Format of the message (printf-like)
 * \param ...    Arguments for the format
 */
void Logger::info(const char *format, ...) const
{
	va_list args;
	va_start (args, format);
	log_message(Logger::INFO, format, args);
	va_end (args);
}

/**
 * Log message at 'debug' level
 * \param format Format of the message (printf-like)
 * \param ...    Arguments for the format
 */
void Logger::debug(const char *format, ...) const
{
	va_list args;
	va_start(args, format);
	log_message(Logger::DEBUG, format, args);
	va_end(args);
}

/**
 * Log message at 'error' level
 * \param format Format of the message (printf-like)
 * \param ...    Arguments for the format
 */
void Logger::error(const char *format, ...) const
{
	va_list args;
	va_start (args, format);
	log_message(Logger::ERROR, format, args);
	va_end (args);
}

/**
 * Log message at 'warning' level
 * \param format Format of the message (printf-like)
 * \param ...    Arguments for the format
 */
void Logger::warning(const char *format, ...) const
{
	va_list args;
	va_start (args, format);
	log_message(Logger::WARNING, format, args);
	va_end (args);
}

/**
 * Log message at 'trace' level
 * \param format Format of the message (printf-like)
 * \param ...    Arguments for the format
 */
void Logger::trace(const char *format, ...) const
{
	va_list args;
	va_start (args, format);
	log_message(Logger::TRACE, format, args);
	va_end (args);
}
